package br.xlabs.daily.service.implementation;

import br.xlabs.daily.model.Post;
import br.xlabs.daily.repository.DailyRepository;
import br.xlabs.daily.service.DailyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DailyServiceImpl implements DailyService {

    @Autowired
    DailyRepository dailyRepository;

    @Override
    public List<Post> findAll() {
        return dailyRepository.findAll();
    }

    @Override
    public Post findById(long id) {
        return dailyRepository.findById(id).get();
    }

    @Override
    public Post save(Post post) {
        return dailyRepository.save(post);
    }

    @Override
    public void delete(Post post) {
        dailyRepository.delete(post);
    }
}
