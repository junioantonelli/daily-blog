package br.xlabs.daily.service;

import br.xlabs.daily.model.Post;

import java.util.List;

public interface DailyService {

    List<Post> findAll();

    Post findById(long id);

    Post save(Post post);

    void delete(Post post);
}
