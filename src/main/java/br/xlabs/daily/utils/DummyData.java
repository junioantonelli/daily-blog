package br.xlabs.daily.utils;

import br.xlabs.daily.model.Post;
import br.xlabs.daily.repository.DailyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class DummyData {

    @Autowired
    DailyRepository dailyRepository;

    @PostConstruct
    public void generateData(){
        List<Post> postList = new ArrayList<>();

        var post1 = new Post();
        post1.setData(LocalDate.now());
        post1.setAutor("Fvlanvs");
        post1.setTitulo("Mussum Ipsum");
        post1.setTexto("Mussum Ipsum, cacilds vidis litro abertis. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. In elementis mé pra quem é amistosis quis leo. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis. Diuretics paradis num copo é motivis de denguis.");

        var post2 = new Post();
        post2.setData(LocalDate.of(2020, 3, 18));
        post2.setAutor("Beltranvs");
        post2.setTitulo("Data Venia");
        post2.setTexto("Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis! Sapien in monti palavris qui num significa nadis i pareci latim. Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget. Paisis, filhis, espiritis santis.");

        var post3 = new Post();
        post3.setData(LocalDate.of(2000, 5, 6));
        post3.setAutor("Ciclanvs");
        post3.setTitulo("Fumus Boni");
        post3.setTexto("Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Si num tem leite então bota uma pinga aí cumpadi! Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. Casamentiss faiz malandris se pirulitá.");

        postList.add(post1);
        postList.add(post2);
        postList.add(post3);

//        postList.stream().forEach((eachPost)->{var temp =dailyRepository.save(eachPost);
//            System.out.println("Dummy Data id: "+temp.getId());}
//        );

    }
}
