package br.xlabs.daily.repository;

import br.xlabs.daily.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface generica para metodos em comum para todos os repositorios
 */
public interface DailyRepository extends JpaRepository<Post, Long> {

}
