package br.xlabs.daily.controller;

import br.xlabs.daily.model.Post;
import br.xlabs.daily.service.DailyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
public class DailyController {

    @Autowired
    DailyService dailyService;

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView getWelcomePage(){
        var mv = new ModelAndView("welcome");
        return mv;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView getLoginPage(){
        var mv = new ModelAndView("login");
        return mv;
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ModelAndView getPosts(){
        var mv = new ModelAndView("posts");
        var posts = dailyService.findAll();
        mv.addObject("posts", posts);
        return mv;
    }

    @RequestMapping(value = "/posts/{id}", method = RequestMethod.GET)
    public ModelAndView getPostDetails(@PathVariable("id") long id){
        var mv = new ModelAndView("postDetails");
        var post = dailyService.findById(id);
        mv.addObject("post", post);
        return mv;
    }

    @RequestMapping(value = "/newpost", method = RequestMethod.GET)
    public ModelAndView getPostForm(){
        var mv = new ModelAndView("postForm");
        return mv;
    }

    @RequestMapping(value = "/newpost", method = RequestMethod.POST)
    public String savePost(@Valid Post post, BindingResult result, RedirectAttributes attributes){
        if(result.hasErrors()){
            attributes.addFlashAttribute("mensagem", "Verifique se os campos obrigatórios foram preenchidos");
            return "redirect:/newpost";
        }
        post.setData(LocalDate.now());
        dailyService.save(post);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/updatePost/{id}", method = RequestMethod.GET)
    public ModelAndView getPostEdit(@PathVariable("id") long id){
        var mv = new ModelAndView("postEdit");
        var post = dailyService.findById(id);
        mv.addObject("post", post);
        return mv;
    }

    @RequestMapping(value = "/updatePost/{id}", method = RequestMethod.POST)
    public String updatePost(@PathVariable("id") long id, @Valid Post post, BindingResult result, RedirectAttributes attributes){
        if(result.hasErrors()){
            attributes.addFlashAttribute("mensagem", "Verifique se os campos obrigatórios foram preenchidos");
            return "redirect:/updatePost/"+id;
        }
        post.setData(LocalDate.now());
        dailyService.save(post);
        attributes.addFlashAttribute("sucesso", "Post editado com sucesso");
        return "redirect:/posts";
    }

    @RequestMapping(value = "/delpost/{id}", method = RequestMethod.GET)
    public String deletePost(@PathVariable("id") long id,  RedirectAttributes attributes){
        var post = dailyService.findById(id);
        try {
            dailyService.delete(post);
            attributes.addFlashAttribute("sucesso", "Post removido com sucesso");
        }catch (IllegalArgumentException e){
            attributes.addFlashAttribute("erro", "Não há post a ser apagado");
        }catch (Exception e1){
            attributes.addFlashAttribute("erro", "Erro ao apagar o post");
        }
        return "redirect:/posts";
    }
}
