# CREATE POSTGRES DOCKER IMAGE

`docker pull postgres`

`mkdir ${HOME}/postgres-data/`

`docker run -d \
	--name dev-postgres \
	-e POSTGRES_PASSWORD=Pass2020! \
	-v ${HOME}/postgres-data/:/var/lib/postgresql/data \
        -p 5432:5432
        postgres`

## USANDO DOCKER COMPOSE
`docker-compose up -d`

## CONECTAR NO TERMINAL DO DOCKER DO POSTGRES
`docker exec -it <CONTAINER_ID OU NAME> bash`

## EXECUTAR O INTERPRETADOR DO POSTGRES
`psql -U <username> <database_name>`

## LIST TABLES
